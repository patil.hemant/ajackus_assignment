Rails.application.routes.draw do
  get 'messages/create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '*unmatched_route', to: 'errors#routing'

  root "home#index"
  namespace :api do
	  namespace :v1 do
	    resources :messages
	  end
	end
end
