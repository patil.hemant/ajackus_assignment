class Api::V1::MessagesController < ApplicationController
	
	def create
		@message = Message.new(message_params)
		if @message.save
    	render :status => 200, :json => {:status => "Success", :data => @message.as_json} 
    else
    	render :status => 401, :json => {:status => "ValidationFail", :data => @message.errors.full_messages} 
    end
	end

	private

	def message_params
		params.require(:message).permit! 
	end

end
