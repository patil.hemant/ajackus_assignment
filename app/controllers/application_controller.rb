class ApplicationController < ActionController::Base
	before_action :set_locale

	private

	def set_locale
	  I18n.locale = extract_locale || I18n.default_locale
	end

	def extract_locale
	  parsed_locale = params[:locale]
	  I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
	end

  def render_404(exception = nil)
    if exception
        logger.info "Rendering 404: #{exception.message}"
    end

    render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
  end

end
