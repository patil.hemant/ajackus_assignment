class SendEmail
  include SuckerPunch::Job

  def perform(message)
  	NotificationMailer.new_message(message).deliver
  end
end

