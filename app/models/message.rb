class Message < ApplicationRecord

	validates :first_name, presence: true, length: { in: 3..25 }
	validates :message_text, presence: true	
	validates :phone_no, format: { with: /\A\d+\z/, message: "Integer only." }

	after_create :send_email

	def send_email
		SendEmail.perform_async(self)
	end

	def user_name
		self.first_name + ' ' + self.last_name 
	end

end
