class NotificationMailer < ApplicationMailer
  default from: 'ashish.blueoort@gmail.com'

  def new_message(message)
    @message = message
    mail(to: 'hemant8905@gmail.com', subject: 'Got A New Message In Your Ajackus Inbox')
  end

end
