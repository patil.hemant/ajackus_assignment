require 'rails_helper'

RSpec.describe Message, :type => :model do
 
  it 'first name can not be blank' do
	  expect(FactoryGirl.create(:message, first_name: nil, last_name: 'Baner', phone_no: '9898787878', message_text: 'lorem').save).to be_falsey
	end	


  it 'is invalid with phone no having aphabates' do
	  expect(FactoryGirl.create(:message, first_name: 'Anil', last_name: 'Baner', phone_no: '987A3546W38', message_text: 'lorem').save).to be_falsey
	end	

end

